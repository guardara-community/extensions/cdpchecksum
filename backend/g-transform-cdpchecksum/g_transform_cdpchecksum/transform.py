#!/usr/bin/env python3

from copy import copy
from dpkt import in_cksum
from struct import pack
from guardara.sdk.transform.transform import TransformInterface

"""
Transform backend skeleton. Please refer to the Developer's Guide[1] on how to 
implement the transform backend component.

[1] https://guardara-community.gitlab.io/documentation/docs/category/developers-guide
"""

class transform(TransformInterface):
    @staticmethod
    def calculate_checksum(value: list) -> list:
        """
        Calculate CDP checksum. Based on:
        https://github.com/boundary/wireshark/blob/master/epan/dissectors/packet-cdp.c
        """
        value = copy(value)
        if len(value) & 1:
            while len(value) == 0 or len(value) % 2 != 0:
                value.append(0)
            value[len(value) - 1] = value[len(value) - 2]
            value[len(value) - 2] = 0
            if value[len(value) - 1] & 0x80:
                value[len(value) - 1] -= 1
                value[len(value) - 2] -= 1
                if value[len(value) - 2] < 0:
                    value[len(value) - 2] = 0
        checksum = in_cksum(bytes(value))
        return list(pack(">H", checksum))

    @staticmethod
    def ensure_zero(value, index):
        """
        Make sure the bytes at index and index + 1 are NULL before checksum
        calculation.
        """
        if not isinstance(index, int):
            raise Exception("Transform requires checksum `index` to be an integer.")
        value = list(value)
        value[index:index + 2] = [0, 0]
        return value

    @staticmethod
    def transform(value, params=None):
        """
        Transform entry point.
        """
        value = transform.ensure_zero(value, params.get("index"))
        checksum = transform.calculate_checksum(value)
        checksum = list(checksum)
        value[params.get("index"):params.get("index") + 2] = checksum
        return bytes(value)
