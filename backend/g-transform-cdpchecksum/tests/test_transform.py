import unittest
from g_transform_cdpchecksum.transform import transform

class TestTransform(unittest.TestCase):

    def test_ensure_zero(self):
        data = b"hello"
        result = transform.ensure_zero(data, 1)
        self.assertEqual(result, [104, 0, 0, 108, 111])

    def test_calculate_checksum(self):
        data = b"hello"
        result = transform.ensure_zero(data, 1)
        self.assertEqual(result, [104, 0, 0, 108, 111])

        result = transform.calculate_checksum(result)
        self.assertEqual(result, [151, 36])

    def test_transform(self):
        data = b"hello"
        result = transform.transform(data, { "index": 1 })
        self.assertEqual(list(result), [104, 151, 36, 108, 111])
