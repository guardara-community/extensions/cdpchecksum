# CDP Checksum Transform

You can use the [GUARDARA SDK](https://guardara-community.gitlab.io/documentation/docs/developers/sdk) to create a deployable package of this transform. Issue the SDK command below from outside of the directory of this repository:

```
guardara extension -e transform -o package -n cdpchecksum  
```

You can upload the resulting package (`g-transform-cdpchecksum.g`) on the Settings / Inventory page of GUARDARA, after which the external engines will pick up and install the extension.

Please note that the built in engine (Destroyer) does not install extensions or run tests. Accordingly, if you wish to test how this extension works:

 * Using the Message Template Designer: The Preview feature uses the built-in Engine (Destroyer) by default. The built-in engine does not install extensions or run tests, therefore, you must first [deploy an external Engine](https://guardara-community.gitlab.io/documentation/docs/deployment#deployment-options) and stop the engine named Destroyer by issuing the command: `docker stop guardara_destroyer`.
 
 * During a test run: The built-in engine does not install extensions or run tests, therefore, you must first [deploy an external Engine](https://guardara-community.gitlab.io/documentation/docs/deployment#deployment-options) and stop the engine named Destroyer by issuing the command `docker stop guardara_destroyer` before running a test.
